# -*- mode: org -*-
#+TITLE:     Introduction to Markdown
#+DATE: June, 2018
#+LANG:    fr
#+STARTUP: overview indent
#+OPTIONS: num:nil toc:t

This document presents a brief overview of the Markdown syntax and
builds on a [[https://guides.github.com/features/mastering-markdown/][presentation from the Github team]] and [[http://csrgxtu.github.io/2015/03/20/Writing-Mathematic-Fomulars-in-Markdown/][blog post from
Archer Reilly]].
* Table of Contents                                         :TOC:
- [[#syntax][Syntax]]
  - [[#headers][Headers]]
  - [[#emphasis][Emphasis]]
  - [[#lists][Lists]]
  - [[#images][Images]]
  - [[#links][Links]]
  - [[#blockquotes][Blockquotes]]
  - [[#inline-code][Inline code]]
- [[#writing-math][Writing Math]]
  - [[#greek-letters][Greek Letters]]
  - [[#usual-functions-and-operators][Usual functions and operators]]
  - [[#exponents-and-indices][Exponents and indices]]
  - [[#fractions-binomial-coefficients-square-roots-][Fractions, binomial coefficients, square roots, ...]]
  - [[#summations-and-integrals][Summations and integrals]]
  - [[#outfits][Outfits]]
- [[#about-markdown][About =markdown=]]
- [[#markdown-and-pandoc-resources-in-english][Markdown/Pandoc Resources in english]]

* Syntax
** Headers
#+BEGIN_EXAMPLE
 # This is an <h1> tag
 ## This is an <h2> tag
 ###### This is an <h6> tag
#+END_EXAMPLE

** Emphasis
#+BEGIN_EXAMPLE
 *This text will be italic*
 _This will also be italic_

 **This text will be bold**
 __This will also be bold__

 _You **can** combine them_
#+END_EXAMPLE

** Lists
*** Unordered
#+BEGIN_EXAMPLE
 - Item 1
 - Item 2
   - Item 2a
   - Item 2b
#+END_EXAMPLE
*** Ordered
#+BEGIN_EXAMPLE
  1. Item 1
  2. Item 2
  3. Item 3
     1. Item 3a
     2. Item 3b
#+END_EXAMPLE
** Images
#+BEGIN_EXAMPLE
 ![GitHub Logo](/images/logo.png)
 Format: ![Alt Text](url)
#+END_EXAMPLE
** Links
#+BEGIN_EXAMPLE
 http://github.com - automatic!
 [GitHub](http://github.com)
#+END_EXAMPLE
** Blockquotes
#+BEGIN_EXAMPLE
 As Kanye West said:

 > We're living the future so
 > the present is our past.
#+END_EXAMPLE
** Inline code
#+BEGIN_EXAMPLE
To print some text with python, you should use the `print()` function.
```
print("Hello world!")
```
#+END_EXAMPLE
* Writing Math
Math formulas are easy to write using Markdown, either using the
*inline* mode or the *displayed formulas* mode. With the inline mode,
formulas are inlined in the current paragraph whereas with the
displayed mode, they appear as centered and emphasized.

The formatting generally slightly differs in both cases since, to
display nicely on a single line, it is generally required to pack them
a bit more than when they are emphasized.

To write formulas using the *inline* mode, they should be surrounded by
a single =$= (as a consequence, whenever you need to use the original
dollar symbol, you should prefix it with a backslash: =\$=). To write
formulas using the *displayed* mode, they should be surrounded by a =$$=. Here are a few examples:

#+BEGIN_EXAMPLE
This expression $\sum_{i=1}^n X_i$ is inlined.
#+END_EXAMPLE

This expression $\sum_{i=1}^n X_i$ is inlined.

#+BEGIN_EXAMPLE
This expression is emphasized:

$$\sum_{i=1}^n X_i$$
#+END_EXAMPLE
This expression is emphasized:

$$\sum_{i=1}^n X_i$$

In the rest of this section we present a brief selection of common
symbols and commands.  Actually, almost any classical LaTeX command
can used as such in Markdown, provided it is surrounded by a =$=. For
more complete examples, please have a look at these ces [[http://www.statpower.net/Content/310/R%2520Stuff/SampleMarkdown.html][examples by
James H. Steiger]].
** Greek Letters
 | Symbol | Command |
 |--------+---------|
 | $\alpha$    | =$\alpha$=     |
 | $\beta$    | =$\beta$=     |
 | $\gamma$    | =$\gamma$=     |
 | $\Gamma$    | =$\Gamma$=     |
 | $\pi$    | =$\pi$=     |
** Usual functions and operators
 | Symbol | Command |
 |--------+---------|
 | $\cos$ | =$\cos$=  |
 | $\sin$ | =$\sin$=  |
 | $\lim$ | =$\lim$=  |
 | $\exp$ | =$\exp$=  |
 | $\to$    | =$\to$=     |
 | $\in$    | =$\in$=     |
 | $\forall$    | =$\forall$=     |
 | $\exists$    | =$\exists$=     |
 | $\equiv$    | =$\equiv$=     |
 | $\sim$    | =$\sim$=     |
 | $\approx$    | =$\approx$=     |
 | $\times$    | =$\times$=     |
 | $\le$    | =$\le$=     |
 | $\ge$    | =$\ge$=     |
** Exponents and indices
 | Symbol | Command   |
 |---------+-----------|
 | $k_{n+1}$  | =$k_{n+1}$= |
 | $n^2$    | =$n^2$=     |
 | $k_n^2$   | =$k_n^2$=   |
** Fractions, binomial coefficients, square roots, ...
 | Symbol                      | Command                     |
 |-----------------------------+-----------------------------|
 | $\frac{4z^3}{16}$            | =$\frac{4z^3}{16}$=           |
 | $\frac{n!}{k!(n-k)!}$       | =$\frac{n!}{k!(n-k)!}$=       |
 | $\binom{n}{k}$              | =$\binom{n}{k}$=              |
 | $\frac{\frac{x}{1}}{x - y}$ | =$\frac{\frac{x}{1}}{x - y}$= |
 | $^3/_7$                       | =$^3/_7$=                     |
 | $\sqrt{k}$                  | =$\sqrt{k}$=                  |
 | $\sqrt[n]{k}$               | =$\sqrt[n]{k}$=               |
** Summations and integrals
 | Symbol                          | Command                              |
 |---------------------------------+--------------------------------------|
 | $\sum_{i=1}^{10} t_i$                     | =$\sum_{i=1}^{10} t_i$=                   |
 | $\int_0^\infty \mathrm{e}^{-x}\,\mathrm{d}x$ | =$\int_0^\infty \mathrm{e}^{-x}\,\mathrm{d}x$= |
** Outfits
 | Symbol                | Command               |
 |-----------------------+-----------------------|
 | $\hat{a}$             | =$\hat{a}$=             |
 | $\bar{a}$             | =$\bar{a}$=             |
 | $\dot{a}$             | =$\dot{a}$=             |
 | $\ddot{a}$            | =$\ddot{a}$=            |
 | $\overrightarrow{AB}$ | =$\overrightarrow{AB}$= |
 
* About =markdown= 
First of all, to know more about =markdown= and its extensions, you may want to read:
- The « [[https://enacit1.epfl.ch/markdown-pandoc/][Élaboration et conversion de documents avec Markdown et Pandoc]] » tutorial by Jean-Daniel Bonjour (EPFL). A must-read in French...
- The wikipedia article on [[https://en.wikipedia.org/wiki/Markdown#Example][Markdown]] provides a good overview of the =markdown= syntax.
- Github proposes a short and efficient introduction: [[https://guides.github.com/features/mastering-markdown/][Mastering Markdown]].

As we explain in the video, =github= and =gitlab= allow you to easily edit =mardown= documents and to render them in =html=. This is quite convenient but may be a bit cumbersome for a daily use. You may prefer to edit these documents with a real editor and later to export them in whichever format you may like (=html=, =pdf=, =docx=, =epub=, etc). There are a few editors specifically designed for =markdown= (see for example the [[https://github.com/jgm/pandoc/wiki/Pandoc-Extras#editors][Editors]]  page of the =pandoc= website) but we rather advise you to use a general-purpose editor that is capable of handling the =markdown= syntax. A few ones were mentioned in the beginning of the video and additional information are available in the [[https://enacit1.epfl.ch/markdown-pandoc/#editeurs_markdown]["Quelques éditeurs adaptés à l'édition Markdown"]] section of Jean-Daniel Bonjour's tutorial.

To convert =markdown= in an "arbitrary" other format, the best solution today is [[http://pandoc.org/][Pandoc]], a software developed by John MacFarlane, a philosopher from de Berkeley, and whose [[https://github.com/jgm/pandoc][main page is on github]]. J.-D. Bonjour's tutorial provides many explanations on how to install and use =pandoc= in the [[https://enacit1.epfl.ch/markdown-pandoc/#commande_pandoc][Utilisation du convertisseur Pandoc]] section. =pandoc= is written in Haskell and may be a bit cumbersome to install. Therefore, we provide here a few alternative solutions:
- Some websites like [[http://www.markdowntopdf.com/]] or [[http://markdown2pdf.com/]] allow you to convert online =markdown= files into =pdf= files without having to install anything on your computer.
- The [[http://commonmark.org/][CommonMark]] project proposes a rigorous specification of the =markdown= syntax and converters =markdown= → =html= / =LaTeX= written in =C= and =JavaScript= ([[https://github.com/CommonMark/CommonMark]]).
- You will find on the website of [[https://daringfireball.net/projects/markdown/][John Gruber]], the creator of =markdown=, a =markdown= → =html= converter written in =perl=.
- [[http://fletcherpenney.net/multimarkdown/][MultiMarkdown]] is another =markdown= extension that provides its own =markdown= → =html= converter written in =C=.
- [[https://github.com/joeyespo/grip][grip]] is a =python=-based server that allows you to convert on the fly =markdown= documents and to preview them with your favorite browser (this is quite useful to avoid useless commits just for the sake of previewing when using =github= ou =gitlab=).
The =pdf= conversion always relies on [[https://fr.wikipedia.org/wiki/LaTeX][LaTeX]], which requires a full-fledged and running LaTeX installation on your computer.
 
In the demo, we show how to generate a =docx= from a =markdown= document with =Pandoc= and we explain that it is then possible to use a word processor like =LibreOffice= to edit the resulting file. Obviously the modifications will not be back-propagated to the original  =markdown= document. You may however want to use =Pandoc= again to convert your new =docx= document to a new =markdown= document.

Another common strategy consists in doing most of the editing of an article/report in =Markdown= and to export it into a =docx= (or =LaTeX=) only in the end so as to prepare it for a camera-ready version with a standard word processing environment (or a =LaTeX= editor).

* Markdown and Pandoc Resources in english
** Mardown et pandoc
- [[https://programminghistorian.org/en/lessons/sustainable-authorship-in-plain-text-using-pandoc-and-markdown][Sustainable Authorship in Plain Text using Pandoc and Markdown by Dennis Tenen and Grant Wythoff]]: in this tutorial, you will first learn the basics of Markdown—an easy to read and write markup syntax for plain text—as well as Pandoc, a command line tool that converts plain text into a number of beautifully formatted file types: PDF, .docx, HTML, LaTeX, slide decks, and more.
- The « [[https://github.com/jgm/pandoc/wiki/Documentation-and-Tutorials][Documentation and Tutorials]] » page of the github Pandoc project provides many links to resources about Pandoc.

** Markdown syntax
- [[https://programminghistorian.org/en/lessons/getting-started-with-markdown][Programming Historian : Getting Started with Markdown]] is very usefull to understand why this language is used, how to format Markdown files, and how to preview Markdown-formatted documents on the web (in spanish [[https://programminghistorian.org/es/lecciones/introduccion-a-markdown][Introducción a Markdown]])
- To write equations with Markdown: [[http://pandoc.org/MANUAL.html#math][see the relevant part of the Pandoc documentation]]
- Writing equations requires a basic knowledge of TeX / LaTeX: [[https://en.wikibooks.org/wiki/LaTeX][see wikibooks.org documentation about LaTeX]]

** Others

The resources listed under the [[https://programminghistorian.org/en/lessons/?activity=sustaining][Sustain section of "The Programming Historian"]] are all relevant for Module 1 (in particular the tutorial on Git/GitHub).

