#+TITLE: Incidence du syndrôme grippal
#+LANGUAGE: fr
#+OPTIONS: *:nil num:1 toc:t

# #+HTML_HEAD: <link rel="stylesheet" title="Standard" href="http://orgmode.org/worg/style/worg.css" type="text/css" />
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>

#+PROPERTY: header-args  :session

* Préface

Pour exécuter le code de cette analyse, il faut disposer des logiciels suivants:

** Emacs 25 ou 26
Une version plus ancienne d'Emacs devrait suffire, mais en ce cas il est prudent d'installer une version récente (9.x) d'org-mode.
** R 3.4
Nous n'utilisons que des fonctionnalités de base du langage R, une version antérieure devrait suffire.

#+BEGIN_SRC emacs-lisp :results output  :exports both
(unless (featurep 'ob-R)
  (print "Veuillez activer R dans org-babel (org-babel-do-languages) !"))
#+END_SRC

Pour la manipulation des dates au format ISO-8601, nous avons besoin du paquet ~parsedate~.
#+BEGIN_SRC R :results output  :exports both
library(parsedate)
#+END_SRC

Enfin, nous allons demander à R d'écrire les nombres décimaux en français — c'est-à-dire avec une virgule entre parties entière et décimale — et d'utiliser un nombre de colonnes plus large que celui utilisé par défaut.
#+BEGIN_SRC R :results silent  :exports both
options(OutDec=",")
options(width=150)
#+END_SRC

* Préparation des données

Les données de l'incidence du syndrome grippal sont disponibles du site Web du [[http://www.sentiweb.fr/][Réseau Sentinelles]]. Nous les récupérons sous forme d'un fichier en format CSV dont chaque ligne correspond à une semaine de la période demandée. Nous téléchargeons toujours le jeu de données complet, qui commence en 1984 et se termine avec une semaine récente. L'URL est:
#+NAME: data-url
http://www.sentiweb.fr/datasets/incidence-PAY-3.csv

Voici l'explication des colonnes donnée sur [[https://ns.sentiweb.fr/incidence/csv-schema-v1.json][le site d'origine:]]

| Nom de colonne | Libellé de colonne                                                                                                                |
|----------------+-----------------------------------------------------------------------------------------------------------------------------------|
| ~week~       | Semaine calendaire (ISO 8601)                                                                                                     |
| ~indicator~  | Code de l'indicateur de surveillance                                                                                              |
| ~inc~        | Estimation de l'incidence de consultations en nombre de cas                                                                       |
| ~inc_low~    | Estimation de la borne inférieure de l'IC95% du nombre de cas de consultation                                                     |
| ~inc_up~     | Estimation de la borne supérieure de l'IC95% du nombre de cas de consultation                                                     |
| ~inc100~     | Estimation du taux d'incidence du nombre de cas de consultation (en cas pour 100,000 habitants)                                   |
| ~inc100_low~ | Estimation de la borne inférieure de l'IC95% du taux d'incidence du nombre de cas de consultation (en cas pour 100,000 habitants) |
| ~inc100_up~  | Estimation de la borne supérieure de l'IC95% du taux d'incidence du nombre de cas de consultation (en cas pour 100,000 habitants) |
| ~geo_insee~  | Code de la zone géographique concernée (Code INSEE) http://www.insee.fr/fr/methodes/nomenclatures/cog/                            |
| ~geo_name~   | Libellé de la zone géographique (ce libellé peut être modifié sans préavis)                                                       |

** Téléchargement
La première ligne du fichier CSV est un commentaire, que nous ignorons en précisant ~skip=1~.

#+BEGIN_SRC R :session :results silent :var url=data-url  :exports both
data = read.csv(trimws(url), skip=1)
#+END_SRC

Regardons ce que nous avons obtenu !
#+BEGIN_SRC R :results output  :exports both
head(data)
tail(data)
#+END_SRC

** Vérification
Il est toujours prudent de vérifier si les données semblent crédibles. Commençons par regarder s'il y a des points manquants dans ce jeu de données:
#+BEGIN_SRC R :results value  :exports both
na_records = apply(data, 1, function(x) any(is.na(x)))
data[na_records,]
#+END_SRC

Voyons aussi comment R a interpreté nos données:
#+BEGIN_SRC R :results output  :exports both
class(data$week)
class(data$inc)
#+END_SRC

Ce sont des entiers, tout va bien !

** Conversions
Pour faciliter les traitements suivants, nous remplaçons les numéros de semaine ISO par les dates qui correspondent aux lundis. D'abord, une petite fonction qui fait le travail:
#+BEGIN_SRC R :results silent  :exports both
convert_week = function(w) {
	ws = paste(w)
	iso = paste0(substring(ws,1,4), "-W", substring(ws,5,6))
	as.Date(parse_iso_8601(iso))
	}
#+END_SRC

Nous appliquons cette fonction à tous les points, créant une nouvelle colonne `date` dans notre jeu de données:
#+BEGIN_SRC R :results output  :exports both
data$date = as.Date(convert_week(data$week))
#+END_SRC

Vérifions qu'elle est de classe `Date`:
#+BEGIN_SRC R :results output  :exports both
class(data$date)
#+END_SRC

Les points sont dans l'ordre chronologique inverse, il est donc utile de les trier:
#+BEGIN_SRC R :results output  :exports both
data = data[order(data$date),]
#+END_SRC

** Vérification des dates
Nous faisons encore une vérification: nos dates doivent être séparées d'exactement une semaine.
#+BEGIN_SRC R :results value  :exports both
all(diff(data$date) == 7)
#+END_SRC

** Inspection
Regardons enfin à quoi ressemblent nos données !
#+BEGIN_SRC R :results output graphics :file inc-plot.png  :exports both
plot(data$date, data$inc, type="l", xlab="Date", ylab="Incidence hebdomadaire")
#+END_SRC

Un zoom sur les dernières années montre mieux la situation des pics en hiver. Le creux des incidences se trouve en été.
#+BEGIN_SRC R :results output graphics :file inc-plot-zoom.png  :exports both
with(tail(data, 200), plot(date, inc, type="l", xlab="Date", ylab="Incidence hebdomadaire"))
#+END_SRC

* Étude de l'incidence annuelle

** Calcul de l'incidence annuelle
Étant donné que le pic de l'épidémie se situe en hiver, à cheval entre deux années civiles, nous définissons la période de référence entre deux minima de l'incidence, du 1er août de l'année /N/ au 1er août de l'année /N+1/. Nous mettons l'année /N+1/ comme étiquette sur cette année décalée, car le pic de l'épidémie est toujours au début de l'année /N+1/. Comme l'incidence du syndrome grippal est très faible en été, cette modification ne risque pas de fausser nos conclusions.

Voici une fonction qui calcule l'incidence annuelle en appliquant ces conventions.
#+BEGIN_SRC R :results silent  :exports both
pic_annuel = function(annee) {
      debut = paste0(annee-1,"-08-01")
      fin = paste0(annee,"-08-01")
      semaines = data$date > debut & data$date <= fin
      sum(data$inc[semaines], na.rm=TRUE)
      }
#+END_SRC

Nous devons aussi faire attention aux premières et dernières années de notre jeux de données. Les données commencent en octobre 1984, ce qui ne permet pas de quantifier complètement le pic attribué à l'année 1985. Nous le supprimons donc de notre analyse. Par contre, pour une exécution en octobre 2018, les données se terminent après le 1er août 2018, ce qui nous permet d'inclure cette année.
#+BEGIN_SRC R :results silent  :exports both
annees <- 1986:2018
#+END_SRC

Nous créons un nouveau jeu de données pour l'incidence annuelle, en applicant la fonction `pic_annuel` à chaque année:
#+BEGIN_SRC R :results value  :exports both
inc_annuelle = data.frame(annee = annees,
                          incidence = sapply(annees, pic_annuel))
head(inc_annuelle)
#+END_SRC

** Inspection
Voici les incidences annuelles en graphique.
#+BEGIN_SRC R :results output graphics :file annual-inc-plot.png  :exports both
plot(inc_annuelle, type="p", xlab="Année", ylab="Incidence annuelle")
#+END_SRC

** Identification des épidémies les plus fortes
Une liste triée par ordre décroissant d'incidence annuelle permet de plus facilement repérer les valeurs les plus élevées:
#+BEGIN_SRC R :results output  :exports both
head(inc_annuelle[order(-inc_annuelle$incidence),])
#+END_SRC

Enfin, un histogramme montre bien que les épidémies fortes, qui touchent environ 10% de la population française, sont assez rares: il y en eu trois au cours des 35 dernières années.
#+BEGIN_SRC R :results output graphics :file annual-inc-hist.png  :exports both
hist(inc_annuelle$incidence, breaks=10, xlab="Incidence annuelle", ylab="Nb d'observations", main="")
#+END_SRC
